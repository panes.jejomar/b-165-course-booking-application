// dependencies and modules
const User = require('../models/User.js');
const bcrypt = require('bcrypt');
const dotenv = require('dotenv');
const req = require('express/lib/request');
const res = require('express/lib/response');
const auth = require('../auth.js')
const Course = require('../models/Course')

dotenv.config();
const salt = parseInt(process.env.SALT);

// create
module.exports.registerUser = (data) => {
   let fName = data.firstName;
   let lName = data.lastName;
   let email = data.email;
   let pw = data.password;
   let sex = data.gender;
   let mobil = data.mobileNo;

   let newUser = new User({
       firstName : fName,
       lastName : lName,
       email : email,
       password : bcrypt.hashSync(pw,salt),
       gender : sex,
       mobileNo : mobil
   });

   return newUser.save().then((user, err) => {
    if(user) {
        return user;
    } else {
        return 'Failed to create new user.'
    }
   });
};


//login user
module.exports.loginUser = (req, res) => {
    console.table(req.body)

    User.findOne({email: req.body.email}).then(foundUser => {
        if (foundUser === null){
            return res.send("User not found")
        } else {

            const isPasswordCorrect = bcrypt.compareSync(req.body.password, foundUser.password)
            console.log(isPasswordCorrect); 
            
            if(isPasswordCorrect){
                return res.send({accessToken: auth.createAccessToken(foundUser)})
            } else {
                return res.send("Incorrect password")
            }
        }
    }).catch(err => {
        res.send(err)
    })
}

//get user details
module.exports.getUserDetails = (req, res) => {
    console.log(req.user)
    //find user via its id
    User.findById(req.user.id).then(result => {
        res.send(result)
    }).catch(err => {
        res.send(err)
    })
}

module.exports.enroll = async (req, res) => {
    console.log(req.user.id) 
    console.log(req.body.courseId) 
 
    if(req.user.isAdmin){
      return res.send("Action Forbidden")
    }


  let isUserUpdated = await User.findById(req.user.id).then(user => {
    console.log(req.user.id)
    console.log("here")

    let newEnrollment = {
        courseId: req.body.courseId
    }

    user.enrollments.push(newEnrollment);

    return user.save().then(user => true).catch(err => err.message)


  })


  if(isUserUpdated !== true) {
      return res.send({message: isUserUpdated})
  }
  
  //find the course we will push for our enrollee array
  let isCourseUpdated = await Course.findById(req.body.courseId).then(course => {
      let enrollee = {
          userId: req.user.id
      }
      course.enrollees.push(enrollee);
      return course.save().then(course => true).catch(err => err.message)
  })
  if(isCourseUpdated !== true) {
      return res.send({message: isCourseUpdated})
  }

  if(isUserUpdated && isCourseUpdated) {
      return res.send({message: "Enrolled Successfully"})
  }

  }
  
  
  //get enrollments
  module.exports.getEnrollments = (req, res) => {
      User.findById(req.user.id).then(result => {
          res.send(result.enrollments)
      }).catch(err => res.send(err))
  }
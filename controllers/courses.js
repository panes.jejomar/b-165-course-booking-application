//dependencies and modules
    const { findByIdAndRemove } = require('../models/Course.js');
const Course = require('../models/Course.js');


//create
module.exports.createCourse = (info) => {
    let cName = info.name;
    let cDesc = info.description;
    let cCost = info.price;

    let newCourse = new Course({
        name: cName,
        description: cDesc,
        price: cCost
    })
    return newCourse.save().then((savedCourse, error) => {
        if(error) {
            return `failed to save new document`;
        } else {
            return savedCourse;
        }
    })
}; // end of createCourse


//retrieve all courses
module.exports.getAllCourses = () => {
    return Course.find({}).then(result => {
        return result;
    })
}; // end of retrieve courses


//get course
    module.exports.getCourse = (id) => {
    return Course.findById(id).then(resultOfQuery => {
        return resultOfQuery;
    })
};

//get all active courses
module.exports.getAllActiveCourses = () => {
    return Course.find({isActive: true}).then(resultOfTheQuery => {
        return resultOfTheQuery;
    });
};

//delete course
module.exports.deleteCourse = (id) => {
    return Course.findByIdAndRemove(id).then((removedCourse, err) => {
        if(err) {
            return 'No Course was removed.'
        } else {
            return 'Course successfully deleted.'
        }
    })
};

//update course
    module.exports.updateCourse = (id, details) => {
        let cName = details.name;
        let cDesc = details.description;
        let cCost = details.price;

        let updatedCourse = {
            name: cName,
            description: cDesc,
            price: cCost
        }
        return Course.findByIdAndUpdate(id, updatedCourse).then((courseUpdated, err) => {
            if(err){
                return 'error'
            } else{
                return 'Course updated Succesfully.'
            }
        })
    };

//deactivate course
module.exports.deactivateCourse = (id) => {
    let updates = {
        isActive: false
    }
    return Course.findByIdAndUpdate(id, updates).then((archived, err) => {
        if(archived){
            return `The Course ${id} has been deactivated.`
        } else {
            return 'error'
        }
    })
};

//deactivate course/s
module.exports.reactivateCourse = (id) => {
    let updates = {
        isActive: true
    }
    return Course.findByIdAndUpdate(id, updates).then((reactivated, err) => {
        if(reactivated){
            return `The Course ${id} has been reactivated.`
        } else {
            return 'error'
        }
    })
};
// dependencieas and modules
const { Router } = require('express');
const express = require('express');
const controller = require('./../controllers/users.js')
const auth = require('../auth.js')

const {verify, verifyAdmin} = auth;

// routing component
const route = express.Router();


// post: create a user
route.post('/register', (req, res) => {
    let userDetails = req.body;
    controller.registerUser(userDetails).then(outcome => {
        res.send(outcome);
    })
})


//login user
route.post('/login', controller.loginUser)

//get user details
    route.get("/getUserDetails", verify, controller.getUserDetails)

//Enroll our registered users
route.post('/enroll', verify, controller.enroll)

//get logged user enrollment
route.get('/getEnrollments', verify, controller.getEnrollments)




//export routing systems
module.exports = route;



// dependencies and modules
const express = require('express');
const { send } = require('express/lib/response');
const controller = require('../controllers/courses.js');
const auth = require('../auth.js')

const {verify, verifyAdmin} = auth;


//routing component
const route = express.Router()

//post routes
    //create course function
route.post('/create', verify, verifyAdmin, (req, res) => {
    let data = req.body;
    controller.createCourse(data).then(outcome => {
        res.send(outcome);
    })
})

//get routes
route.get('/all', verify, verifyAdmin, (req, res) => {
    controller.getAllCourses().then(outcome => {
        res.send(outcome);
    })
})

route.get('/:id', (req, res) => {
    controller.getCourse().then(outcome => {
        let courseId = req.params.id;

        controller.getCourse(courseId).then(outcome => {
            res.send(outcome)
        })
    })
})

route.get('/', (req, res) => {
    controller.getAllActiveCourses().then(outcome => {
        res.send(outcome);
    })
})

//del routes
route.delete('/:id', verify, verifyAdmin, (req, res) => {
    let id = req.params.id;
    controller.deleteCourse(id).then(outcome => {
        res.send(outcome);
    });
});

//put routes
route.put('/:id', verify, verifyAdmin, (req, res) => {
    let id = req.params.id;
    let details = req.body

    //data validation
        let cName = details.name;
        let cDesc = details.description;
        let cCost = details.price;
        if(
            cName !== '' &&
            cDesc !== '' &&
            cCost !== ''
        ){
            controller.updateCourse(id, details).then(outcome => {
                res.send(outcome);
            })
        } else {
            res.send('Incorrect Input, Make sure details are complete.')
        };
})

//deactivate course
route.put('/:id/archive', verify, verifyAdmin, (req, res) => {
    let courseId = req.params.id;
    controller.deactivateCourse(courseId).then(resultOfTheFunction => {
        res.send(resultOfTheFunction)
    })
});

//deactivate course
route.put('/:id/reactivate', verify, verifyAdmin, (req, res) => {
    let courseId = req.params.id;
    controller.reactivateCourse(courseId).then(resultOfTheFunction => {
        res.send(resultOfTheFunction)
    })
});

//export routing system
module.exports = route;
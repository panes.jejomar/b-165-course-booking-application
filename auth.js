// module which will have the methods that will help us authorize our users to access or to disallow access to certain parts/ feature in our app.

//packages and dependencies
const jwt = require("jsonwebtoken");
const User = require("./models/User");
const secret = "CourseBookingAPI";

//function for token creation
module.exports.createAccessToken = (userData) => {
    //payload
    const data = {
        id: userData._id,
        email: userData.email,
        isAdmin: userData.isAdmin
    };
    //creates the signature
    //paylaod
    //signature
    return jwt.sign(data, secret, {
        //expiration
        //expiresIn: '4h'
    })
};

module.exports.verify = (req, res, next) => {

	console.log(req.headers.authorization);
	let token = req.headers.authorization;

	if(typeof token === "undefined"){

		return res.send({auth: "Failed. No Token"});
	
	} else {

		console.log(token);

		token = token.slice(7, token.length);

		console.log(token);

		jwt.verify(token, secret, function(err, decodedToken){

			if(err){
				return res.send({
					auth: "Failed",
					message: err.message
				});

			} else {

				console.log(decodedToken);
				req.user = decodedToken

				next();
			}
		})
	}
};

module.exports.verifyAdmin = (req, res, next) => {

	if(req.user.isAdmin){

		next();

	} else {

		return res.send({
			auth: "Failed",
			message: "Action Forbidden"
		})
	}
}

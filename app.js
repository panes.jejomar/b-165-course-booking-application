// packages and dependencies
const express = require("express");
const mongoose = require("mongoose");
const dotenv = require("dotenv");
const courseRoutes = require('./routes/courses');
const usersRoutes = require('./routes/users');

// server setup
const app = express();
dotenv.config();
app.use(express.json())
const secret = process.env.CONNECTION_STRING;
const port = process.env.PORT;

// application routes

app.use('/courses', courseRoutes);
app.use('/users', usersRoutes);

// database connection
mongoose.connect(secret);
let dbStatus = mongoose.connection;
dbStatus.on('open', () => {
    console.log(`database is connected`);
})

// gateway response
app.get('/', (req, res) => {
    res.send(`Welcome to Jejomar's Nespafe PH`)
});

app.listen(port, () => {
    console.log(`server is running on ${port}`)
});



//business logic

